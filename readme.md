## Instructions

First you must create a bitbucket account along with a repository. Fork [this repository](http://bitbucket.org/MrPiTimesE/competitive-programming-questions/) and make appropriate changes as described below. Commit and push the changes. Then give the account MrPiTimesE read access to the repository. 

Create a problem with the concepts tagged along with difficulty level (easy, medium hard.) The problem that you create should be uploaded either in a markdown format, or html file. You are also to generate a large number of test cases for the problem you create. See folder for problem 0 as an example of a submission. The test cases that should be generated should be a lot more. But the size of each stdin/stdout should not exceed 5 MB. Also, follow the folder structure as outlined below.

Feel free to use [test-case-generator](http://test-case-generator.herokuapp.com/).

Use redirection to generate

```
$ python solution.py < test_cases/2/in.txt > test_cases/2/out.txt

```


```
.
├── problem-statements
│   ├── 0
│   │   ├── difficulty.txt
│   │   ├── problem.md
│   │   ├── solution.py
│   │   ├── tags.txt
│   │   └── test_cases
│   │       ├── 1
│   │       │   ├── in.txt
│   │       │   └── out.txt
│   │       └── 2
│   │           ├── in.txt
│   │           └── out.txt
│   
└── readme.md


```